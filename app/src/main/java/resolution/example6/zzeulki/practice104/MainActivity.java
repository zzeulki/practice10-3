package resolution.example6.zzeulki.practice104;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(new CanvasView(this));
    }

    protected class CanvasView extends View {
        public CanvasView(Context context){
            super(context);
        }

        public void onDraw(Canvas canvas){
            canvas.drawColor(Color.WHITE);
            Paint paint_L = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint eye1 = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint eye2 = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint mouth = new Paint(Paint.ANTI_ALIAS_FLAG);
            Paint name = new Paint(Paint.ANTI_ALIAS_FLAG);

            int[] color_list = {Color.YELLOW, Color.WHITE,Color.YELLOW, Color.WHITE,Color.YELLOW, Color.WHITE,Color.YELLOW};
            float[] post_list = {0.15f,0.30f,0.45f,0.60f,0.75f,0.90f,1.0f};



            paint.setShader(new SweepGradient(550,800,
                   color_list, post_list));
            canvas.drawCircle(500,500,5000,paint);

            paint_L.setColor(Color.BLACK);
            canvas.drawCircle(550,800,430,paint_L);

            paint2.setColor(Color.WHITE);
            canvas.drawCircle(550,800,400,paint2);

            eye1.setColor(Color.BLACK);
            canvas.drawArc(new RectF(300,600,400,700),30,210,false,eye1);

            eye2.setColor(Color.BLACK);
            canvas.drawArc(new RectF(600,600,700,700),-50,210,false,eye2);

            mouth.setColor(Color.RED);
            canvas.drawArc(new RectF(330,750,650,1000),-20,210,false,mouth);

            String msg = "2013105001 강슬기";

            name.setColor(Color.BLACK);
            name.setTextSize(40);
            canvas.drawText(msg,550,1400,name);


        }
    }
}
